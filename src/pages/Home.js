import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home(){

	const data = {
		title: "Online Street Foods",
		content: "Affordable food for every filipino's!",
		destination: "/products",
		label: "Order now!"
	}

	return(
		<>
			<Banner bannerProp={data}/>
			<Highlights/>
		</>
	)
}