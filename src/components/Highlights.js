import {Row, Col, Card} from 'react-bootstrap'


export default function Highlights(){
  return(
    <Row className = "mt-3 mb-3">
    <Col xs={12} md={4}>
      <Card className = "cardHighlight p-3">
        <Card.Body>
          <Card.Title>FOOD!</Card.Title>
          <Card.Text>
            There are so many choices in Food Category like barbeque, kwek-kwek, siomai, siopao, balut, fishball, crispy proben, crispy isaw and etc..
          </Card.Text>
        </Card.Body>
      </Card>
    </Col>
    <Col xs={12} md={4}>
      <Card className = "cardHighlight p-3">
        <Card.Body>
          <Card.Title>DRINKS!</Card.Title>
          <Card.Text>
            We have also a refreshing drinks like lemonade, choco juice, melon juice, pineapple juice and etc.. All are natural flavor!
          </Card.Text>
        </Card.Body>
      </Card>
    </Col>
    <Col xs={12} md={4}>
      <Card className = "cardHighlight p-3">
        <Card.Body>
          <Card.Title>FAST DELIVERY!</Card.Title>
          <Card.Text>
            We accept delivery all over the philippines to serve our customers and give them a satisfaction.
          </Card.Text>
        </Card.Body>
      </Card>
    </Col>
    </Row>
    )
}

